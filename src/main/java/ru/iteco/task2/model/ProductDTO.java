package ru.iteco.task2.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
@ApiModel(description = "")
public class ProductDTO {
    @ApiModelProperty("id")
    private Long id;

    @ApiModelProperty("name")
    private String name;

    @ApiModelProperty("amount")
    private Long amount;

    @ApiModelProperty("price")
    private Double price;

    @ApiModelProperty("parentId")
    private Long parentId;
}
