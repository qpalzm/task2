package ru.iteco.task2.service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import ru.iteco.task2.entity.CustomUser;
import ru.iteco.task2.entity.UserEntity;
import ru.iteco.task2.model.UserDao;
import ru.iteco.task2.repository.UserRepository;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Service
public class CustomDetailsService implements UserDetailsService {

  private UserRepository userRepository;

  @Autowired
  public CustomDetailsService(UserRepository userRepository) {
    this.userRepository = userRepository;
  };

  @Override
  public CustomUser loadUserByUsername(final String username) throws UsernameNotFoundException {
    UserDao userDao = null;
    try {
      userDao = getUserDetails(username);
      CustomUser customUser = new CustomUser(userDao);
      return customUser;
    } catch (Exception e) {
      e.printStackTrace();
      throw new UsernameNotFoundException("User " + username + " was not found in the database");
    }
  }

  private UserDao getUserDetails(String username) {
    Collection<GrantedAuthority> grantedAuthoritiesList = new ArrayList<>();
    List<UserEntity> userList = userRepository.findAllByUsernameOrEmail(username, username);

    if(!userList.isEmpty()) {
      GrantedAuthority grantedAuthority = new SimpleGrantedAuthority("ROLE_ADMIN");
      grantedAuthoritiesList.add(grantedAuthority);
      var userEntity = userList.get(0);
      return UserDao.builder()
              .username(userEntity.getUsername())
              .email(userEntity.getEmail())
              .password(userEntity.getPassword())
              .grantedAuthoritiesList(grantedAuthoritiesList)
              .build();
    }

    return null;
  }

}
