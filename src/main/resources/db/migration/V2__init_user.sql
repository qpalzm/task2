CREATE TABLE USERS (
    ID bigserial PRIMARY KEY,
    USERNAME VARCHAR(45),
    EMAIL VARCHAR(45),
    PASSWORD VARCHAR(60)
);

INSERT INTO USERS (USERNAME, EMAIL, PASSWORD) VALUES (
    'admin','tutorialspoint@gmail.com','$2a$08$fL7u5xcvsZl78su29x1ti.dxI.9rYO8t0q5wk2ROJ.1cdR53bmaVG'
);

INSERT INTO USERS (USERNAME, EMAIL, PASSWORD) VALUES (
    'test','myemail@gmail.com','$2a$08$fL7u5xcvsZl78su29x1ti.dxI.9rYO8t0q5wk2ROJ.1cdR53bmaVG'
);