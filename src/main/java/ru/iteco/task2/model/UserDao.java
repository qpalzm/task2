package ru.iteco.task2.model;

import lombok.Builder;
import lombok.Data;
import org.springframework.security.core.GrantedAuthority;

import javax.persistence.Column;
import java.util.ArrayList;
import java.util.Collection;

@Data
@Builder
public class UserDao {

    private Long id;

    private String username;

    private String email;

    private String password;

    private Collection<GrantedAuthority> grantedAuthoritiesList = new ArrayList<>();
}
