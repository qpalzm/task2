package ru.iteco.task2.model;

import io.swagger.annotations.ApiModel;

@ApiModel(value = "CurrencyPaymentDocumentSortField_v4", description = "Значения фильтрации по полям валютно-платежного документа ИКБ 2.0")
public enum EdgeChildType {
    PRODUCT,
    PRODUCT_GROUP,
    CLUSTER
}
