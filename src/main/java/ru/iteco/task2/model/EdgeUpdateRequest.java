package ru.iteco.task2.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@ApiModel(description = "")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class EdgeUpdateRequest {
    @ApiModelProperty("childType")
    EdgeChildType childType;

    @ApiModelProperty("edges")
    List<EdgeDto> edges;
}
