package ru.iteco.task2.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.iteco.task2.entity.GroupHistory;

@Repository
public interface GroupHistoryRepository extends CrudRepository<GroupHistory, Long> {

}
