# task2

Test Task 2

# Пример запроса изменения связей графа:

* POST: http://localhost:8080/graph/edges
{
    "edgeUpdateRequests": [
        {   
            "childType": "PRODUCT", 
            "edges":[{
                "childId": 8,
                "parentId": 1
            }]
        }
    ]
}

# Пример получения графа целиком по идентификатору вершины и дате:
GET: http://localhost:8080/graph?id=2?date=2021-04-01T00:00
{
"id": 2,
"name": "food",
"type": "CLUSTER",
"children": [
    {
    "id": 2,
    "name": "bakery",
    "type": "PRODUCT_GROUP",
    "children": [
        {
        "id": 7,
        "name": "chicken",
        "type": "PRODUCT",
        "children": null
        },
        {
        "id": 5,
        "name": "dress",
        "type": "PRODUCT",
        "children": null
        }
    ]
}]
}
# Пример итеративного получения вершин графа:

* кластеры
GET: http://localhost:8080/clusters
[
    {
        "id": 1,
        "name": "default"
    },
    {
        "id": 2,
        "name": "food"
    },
    {
        "id": 3,
        "name": "clothes"
    }
]
* группы
GET: http://localhost:8080/clusters/3/groups
[
    {
        "id": 2,
        "name": "bakery",
        "parentId": 2
    },
    {
        "id": 3,
        "name": "meat",
        "parentId": 2
    },
    {
        "id": 4,
        "name": "milk products",
        "parentId": 2
    },
    {
        "id": 5,
        "name": "desert",
        "parentId": 2
    }
]
* продукты
GET: http://localhost:8080/clusters/3/groups/3/products
[
    {
        "id": 6,
        "name": "boots",
        "amount": 6,
        "price": 600.0,
        "parentId": 3
    }
]

