package ru.iteco.task2.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
@ApiModel(description = "")
public class Node {
    @ApiModelProperty("ID")
    private Long id;

    @ApiModelProperty("name")
    private String name;

    @ApiModelProperty("type")
    private EdgeChildType type;

    @ApiModelProperty("children")
    private List<Node> children;
}
