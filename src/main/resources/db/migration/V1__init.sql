SET search_path TO public5;

CREATE TABLE cluster
(
    id     bigserial PRIMARY KEY,
    name   varchar   NOT NULL
);

CREATE TABLE product_group
(
    id     bigserial PRIMARY KEY,
    name   varchar   NOT NULL,
    parent_id int8 NULL
);

CREATE TABLE product
(
    id     bigserial PRIMARY KEY,
    name   varchar   NOT NULL,
    amount int8 NULL,
    price  numeric(10,2) NULL,
    parent_id int8 NOT NULL
);

INSERT INTO cluster ("name") VALUES
('default'),
('food'),
('clothes');

INSERT INTO product_group ("name", parent_id) VALUES
('default', 1),
('bakery', 2),
('meat', 2),
('milk products', 2),
('desert', 2),
('shoes', 3),
('bags', 3),
('hats', 3),
('clothes', 3);

INSERT INTO product (amount, "name", price, parent_id) VALUES
(1,'butter',100.0, 4),
(2,'chapeau',200.0, 8),
(3,'cap',300.0, 8),
(4,'cake',500.0, 5),
(5,'dress',123.0, 9),
(6,'boots',600.0, 3),
(7,'chicken',400.0, 2),
(8,'veal',500.0, 3);