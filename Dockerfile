FROM adoptopenjdk:11-jre-hotspot
MAINTAINER  iteco.ru
VOLUME /tmp
EXPOSE 8080
ADD target/task2-0.0.1-SNAPSHOT.jar task2.jar
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/task2.jar"]