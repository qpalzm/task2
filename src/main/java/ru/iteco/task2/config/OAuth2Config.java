package ru.iteco.task2.config;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;
import org.springframework.security.oauth2.provider.token.store.JwtTokenStore;

@Configuration
public class OAuth2Config extends AuthorizationServerConfigurerAdapter{
  private String clientid = "task02";
  //private String clientSecret = "my-secret-key";
  private String clientSecret = "$2y$12$69w0BnZC5cQgsMlescU59.9P.8oZ0NzuCisYoPkGO.FV2QDulnIjG";
  private String privateKey = "-----BEGIN RSA PRIVATE KEY-----\n" +
      "MIIEpAIBAAKCAQEAsTgg4gnFIhkSp93uKmXfFZzuAppNySFkTcq8Wu4JrFE6a/sx\n" +
      "0oxzjgqcXGn0U58BlBjS0fpqM7HrpZM2rQ9nKvbc7kQoqUvduoa7fQ2l0JHzgUBH\n" +
      "TJmBwzWLiC0ZxadItm2Y8Q/7T6mHz/d42Y4ZPWCw1HE0QxAATDTESza5+yjBZQ5Y\n" +
      "Liu+KEg/h4TsXPIJac/zMtynX1AgTgkr8Q+1rc9HnxTUl+yZLdV5XpCNX3SfPbr3\n" +
      "IIMjY21SfldNM+7jYZj5olSgOxO3jTQK9xJnuHYu60PNjtxK0mCVTvNRUp0WJTDj\n" +
      "P6HUI3dDOjoo4uyKEQR6Cxo/PJ7cDc6w8nH7PQIDAQABAoIBAHkKlxsMBUMG0QEq\n" +
      "ybLXpYkDiGTqs1o+nl2rrP6GOsEhMb/y79KY74wCn01XaPN8VP2r/sJAQS1Rxltu\n" +
      "sxd8v8BOoBN1JjiLgsQVphUBr1FHfoO3PXuKFWEZP1YhR8sCON2w+ZccjU+ZwRNA\n" +
      "OaCON5r+YYiYUVPEqA9ksvWUSW7LAgLVRDyYCfGKQRnynNblYHDw5tTQsGZh0xlb\n" +
      "jkTKwB+CTSyflyzJQgA5uE4149emr+NV5OOnJqwNENjIOMHWcWHCuQYwO0vndBI7\n" +
      "dSJC4Jjg6yv6uflg24iJ91xKdz9gA5icORUh4lfDFiLKD3Tg7oShxno6DMXk4DTA\n" +
      "GE0JKlkCgYEA4EtqtZ5sFbV7jcwG9QT8NBP5CouTXbHd4wTGEXhBLEWJIZKkPVOq\n" +
      "1hgCk1f/9YS8TsenooNIcpRtYDUJRROA6iidwTbjgY5Uxc1dty4PM4DFZwhkJKeo\n" +
      "gX0WEVRSORqOis6crE4nmPTj4Kqp2sbQKYM30ebXzUvIYRfjetZK8VcCgYEAykUw\n" +
      "V6/3/QlwxctBcRhrYHf5nSO8yNeVZGX36/ksNgJm/tU5DykTut00uiA+HIp9KH7r\n" +
      "JYaATViO1vMiWgNP89huJc3t/pEYZEtFwEpPicEc3FsIh2w2lWgn2MIOe0Cg2H9J\n" +
      "76zRxXMmxIqwmvsnq5GPmrl68cZ2BVHzDVCK94sCgYAf2HVhLgoYlvjg7Bv8w+Vv\n" +
      "G3rblW+p+OwRJIdeyZ0vRPwwaRqqnmt+FFChTPhJirwVz2EswMkTg6F37G/Rf/Bo\n" +
      "zrV/TmTE60l8ACmTLZX48QqWE2nsjmVtZS4x7a/y7aNZb/ZsvHgAzpxgNLsDFgFO\n" +
      "M5REWCtBucXMi1DACxb+5wKBgQDEoBWqaCkOFYtG+5sWMcLP+FDybLcsFkXnXyhu\n" +
      "f752kkdFGklpb2j6keabuJQocofQQfCcfe9jQN86HQgbsHScepw4q8LbYHjxpiqc\n" +
      "DqDbLJq9qtDTtGIrQyDZGWuybNTvIG3jS4BwDMx26stbHSMnKU0L/5qARYidC1jW\n" +
      "bKFqmQKBgQChyb3AdJzCbq7KSEu7jIasnQowi08KNpWMawaONYwvOT/5R82eRh1x\n" +
      "zJTh1763lAEdryqBi/AUvAknoUn9nprX1ebNy/OZwWAnf0RWcI1F2+6nDFt/PPHm\n" +
      "ZOfBmITwDwhkvc218wjtxSxEQhPOPtFGj4hg6JMe39vzeQ1PSp9xug==\n" +
      "-----END RSA PRIVATE KEY-----\n";
  private String publicKey = "-----BEGIN PUBLIC KEY-----\n" +
      "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAsTgg4gnFIhkSp93uKmXf\n" +
      "FZzuAppNySFkTcq8Wu4JrFE6a/sx0oxzjgqcXGn0U58BlBjS0fpqM7HrpZM2rQ9n\n" +
      "Kvbc7kQoqUvduoa7fQ2l0JHzgUBHTJmBwzWLiC0ZxadItm2Y8Q/7T6mHz/d42Y4Z\n" +
      "PWCw1HE0QxAATDTESza5+yjBZQ5YLiu+KEg/h4TsXPIJac/zMtynX1AgTgkr8Q+1\n" +
      "rc9HnxTUl+yZLdV5XpCNX3SfPbr3IIMjY21SfldNM+7jYZj5olSgOxO3jTQK9xJn\n" +
      "uHYu60PNjtxK0mCVTvNRUp0WJTDjP6HUI3dDOjoo4uyKEQR6Cxo/PJ7cDc6w8nH7\n" +
      "PQIDAQAB\n" +
      "-----END PUBLIC KEY-----\n";

  @Autowired
  @Qualifier("authenticationManagerBean")
  private AuthenticationManager authenticationManager;

  @Bean
  public JwtAccessTokenConverter tokenEnhancer() {
    JwtAccessTokenConverter converter = new JwtAccessTokenConverter();
    converter.setSigningKey(privateKey);
    converter.setVerifierKey(publicKey);
    return converter;
  }
  @Bean
  public JwtTokenStore tokenStore() {
    return new JwtTokenStore(tokenEnhancer());
  }
  @Override
  public void configure(AuthorizationServerEndpointsConfigurer endpoints) throws Exception {
    endpoints.authenticationManager(authenticationManager).tokenStore(tokenStore())
        .accessTokenConverter(tokenEnhancer());
  }
  @Override
  public void configure(AuthorizationServerSecurityConfigurer security) throws Exception {
    security.tokenKeyAccess("permitAll()").checkTokenAccess("isAuthenticated()");
  }
  @Override
  public void configure(ClientDetailsServiceConfigurer clients) throws Exception {
    clients.inMemory().withClient(clientid).secret(clientSecret).scopes("read", "write")
        .authorizedGrantTypes("password", "refresh_token").accessTokenValiditySeconds(20000)
        .refreshTokenValiditySeconds(20000);

  }
}
