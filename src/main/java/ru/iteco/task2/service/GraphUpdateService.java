package ru.iteco.task2.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.iteco.task2.model.GraphUpdateRequest;
import ru.iteco.task2.model.Node;
import ru.iteco.task2.repository.GraphRepository;
import ru.iteco.task2.repository.GroupRepository;
import ru.iteco.task2.repository.ProductRepository;

import java.time.LocalDateTime;

@Service
public class GraphUpdateService {
    private GroupRepository groupRepository;
    private ProductRepository productRepository;
    private GraphRepository graphRepository;
    @Autowired
    public GraphUpdateService(GroupRepository groupRepository,
                              ProductRepository productRepository,
                              GraphRepository graphRepository) {
        this.groupRepository = groupRepository;
        this.productRepository = productRepository;
        this.graphRepository = graphRepository;
    }

    // TODO: add swagger
    // TODO: add error handling

    // TODO: check node deletion with history
    // TODO: add whole graph save
    // TODO: think of graph edge store refactoring
    public void updateGraph(GraphUpdateRequest graphUpdateRequest) {
        graphRepository.updateGraph(graphUpdateRequest);
    }

    public Node getGraph(Long id, LocalDateTime dateTime) {
        return graphRepository.getGraph(id, dateTime);
    };
}
