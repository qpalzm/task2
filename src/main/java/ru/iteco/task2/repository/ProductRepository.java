package ru.iteco.task2.repository;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.iteco.task2.entity.Product;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Repository
public interface ProductRepository extends CrudRepository<Product, Long> {
    List<Product> getProductsByParentId(Long parentId);
    Optional<Product> getById(Long id);

    @Modifying
    @Query("update product p set p.parentId = :parentId where p.id = :id")
    void updateParentIdById(@Param(value = "id") long id, @Param(value = "parentId") Long parentId);


    @Query(value = "with changed_rows as (select ph1.* \n" +
            "from product_history ph1 JOIN(\n" +
            "SELECT ph.product_id, min(ph.updated) as updated \n" +
            "FROM product_history ph \n" +
            "where ph.updated >=?1 group by ph.product_id \n" +
            ") tmp ON tmp.product_id = ph1.product_id AND tmp.updated = ph1.updated) \n" +
            "\n" +
            "select p.* \n" +
            "from product p where not exists (select * from changed_rows cr where cr.product_id = p.id) \n" +
            "union \n" +
            "select changed_rows.product_id as id, changed_rows.\"name\", changed_rows.amount, changed_rows.price, changed_rows.parent_id from changed_rows", nativeQuery = true)
    List<Product> getActualProductsForDate(LocalDateTime date);
}
