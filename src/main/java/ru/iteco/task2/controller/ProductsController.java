package ru.iteco.task2.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.iteco.task2.EntityMapper;
import ru.iteco.task2.entity.Product;
import ru.iteco.task2.model.ProductDTO;
import ru.iteco.task2.repository.ProductRepository;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/products")
@Api(value = "Интерфейс для взаимодействия с продуктами")
@Log4j2
public class ProductsController {

    private ProductRepository productRepository;

    @Autowired
    public ProductsController(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    @ApiOperation("")
    @GetMapping(produces = {"application/json"})
    public ResponseEntity<List<Product>> getProducts(
            @ApiParam(value = "", required = true) @RequestParam(required = false) Long productGroupId) {
        return ResponseEntity.ok(productRepository.getProductsByParentId(productGroupId));
    }

    @ApiOperation("Получение продукта по идентификатору")
    @GetMapping(value = "/{id}", produces = {"application/json"})
    public ResponseEntity<ProductDTO> getProduct(
            @ApiParam(value = "", required = true) @PathVariable("id") Long id) {
        var productOptional = productRepository.getById(id);
        if(productOptional.isPresent()) {
            return ResponseEntity.ok(EntityMapper.INSTANCE.toDTO(productOptional.get()));
        }
        return ResponseEntity.notFound().build();
    }

    @ApiOperation("")
    @DeleteMapping(value = "/{id}", produces = {"application/json"})
    public ResponseEntity<ProductDTO> deleteProduct(
            @ApiParam(value = "", required = true) @PathVariable("id") Long id) {
        productRepository.deleteById(id);
        return ResponseEntity.ok().build();
    }

    @ApiOperation("")
    @PutMapping(consumes = {"application/json"}, produces = {"application/json"})
    public ResponseEntity<ProductDTO> createProduct(@RequestBody ProductDTO product) {
        return ResponseEntity.ok(EntityMapper.INSTANCE.toDTO(productRepository.save(EntityMapper.INSTANCE.toEntity(product))));
    }

    @ApiOperation("")
    @PostMapping(produces = {"application/json"})
    public ResponseEntity<ProductDTO> updateProduct(@RequestBody ProductDTO product) {
        return ResponseEntity.ok(EntityMapper.INSTANCE.toDTO(productRepository.save(EntityMapper.INSTANCE.toEntity(product))));
    }
}
