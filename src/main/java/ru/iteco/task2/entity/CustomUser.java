package ru.iteco.task2.entity;

import org.springframework.security.core.userdetails.User;
import ru.iteco.task2.model.UserDao;

public class CustomUser extends User {
  private static final long serialVersionUID = 1L;
  public CustomUser(UserDao user) {
    super(user.getUsername(), user.getPassword(), user.getGrantedAuthoritiesList());
  }
}
