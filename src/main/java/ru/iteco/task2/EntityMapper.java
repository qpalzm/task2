package ru.iteco.task2;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;
import ru.iteco.task2.entity.GroupHistory;
import ru.iteco.task2.entity.Product;
import ru.iteco.task2.entity.ProductGroup;
import ru.iteco.task2.entity.ProductHistory;
import ru.iteco.task2.model.ProductDTO;

@Mapper
public interface EntityMapper {

    EntityMapper INSTANCE = Mappers.getMapper(EntityMapper.class);

    @Mapping(target = "productId", source = "id")
    @Mapping(target = "id", source = "id", ignore = true)
    ProductHistory map(Product source);

    Product mapToProduct(ProductHistory source);

    @Mapping(target = "id", source = "id", ignore = true)
    @Mapping(target = "productGroupId", source = "id")
    GroupHistory map(ProductGroup source);

    ProductDTO toDTO(Product product);

    Product toEntity(ProductDTO product);

}