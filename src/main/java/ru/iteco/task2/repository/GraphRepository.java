package ru.iteco.task2.repository;

import org.springframework.stereotype.Repository;
import ru.iteco.task2.EntityMapper;
import ru.iteco.task2.entity.Cluster;
import ru.iteco.task2.entity.Product;
import ru.iteco.task2.entity.ProductGroup;
import ru.iteco.task2.entity.ProductHistory;
import ru.iteco.task2.model.*;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

@Repository
@Transactional
public class GraphRepository {
    private ProductRepository productRepository;
    private GroupRepository groupRepository;
    private ClusterRepository clusterRepository;
    private GroupHistoryRepository groupHistoryRepository;
    private ProductHistoryRepository productHistoryRepository;

    public GraphRepository(ProductRepository productRepository,
                           GroupRepository groupRepository,
                           ClusterRepository clusterRepository,
                           GroupHistoryRepository groupHistoryRepository,
                           ProductHistoryRepository productHistoryRepository) {
        this.productRepository = productRepository;
        this.groupRepository = groupRepository;
        this.clusterRepository = clusterRepository;
        this.productHistoryRepository = productHistoryRepository;
        this.groupHistoryRepository = groupHistoryRepository;
    }

    public void updateGraph(GraphUpdateRequest graphUpdateRequest) {
        for (EdgeUpdateRequest edgeUpdateRequest: graphUpdateRequest.getEdgeUpdateRequests()) {
            if(EdgeChildType.PRODUCT.equals(edgeUpdateRequest.getChildType())) {
                edgeUpdateRequest.getEdges()
                        .stream()
                        .forEach(it ->
                        {
                            Optional<Product> productOptional = productRepository.getById(it.getChildId());
                            if(productOptional.isPresent())
                            {
                                var previousState = productOptional.get();
                                productHistoryRepository.save(EntityMapper.INSTANCE.map(previousState));
                            }
                            productRepository.updateParentIdById(it.getChildId(), it.getParentId());
                        });
            }
            if(EdgeChildType.PRODUCT_GROUP.equals(edgeUpdateRequest.getChildType())) {
                edgeUpdateRequest.getEdges()
                        .stream()
                        .forEach(it -> {
                            Optional<ProductGroup> productGroupOptional = groupRepository.getById(it.getChildId());
                            if (productGroupOptional.isPresent()) {
                                var previousState = productGroupOptional.get();
                                groupHistoryRepository.save(EntityMapper.INSTANCE.map(previousState));
                            }
                            groupRepository.updateParentIdById(it.getChildId(), it.getParentId());
                        });
            }
        }
    }

    public Node getGraph(Long id, LocalDateTime dateTime) {
        Node result = null;
        Map<Long, List<Product>> groupToProductsMap = new HashMap<>();
        Map<Long, List<ProductGroup>> clusterToGroupMap = new HashMap<>();
        if(dateTime != null){
            var actualGroups = groupRepository.getProductGroupsByDate(dateTime);
            for (ProductGroup productGroup: actualGroups) {
                if(clusterToGroupMap.putIfAbsent(productGroup.getParentId(), new ArrayList<ProductGroup>() {
                    {add(productGroup);}
                }) != null) {
                    clusterToGroupMap.get(productGroup.getParentId()).add(productGroup);
                };
            }
            var actualProducts = productRepository.getActualProductsForDate(dateTime);
            for (Product product: actualProducts) {
                if(groupToProductsMap.putIfAbsent(product.getParentId(), new ArrayList<Product>() {
                    {add(product);}
                }) != null) {
                    groupToProductsMap.get(product.getParentId()).add(product);
                };
            }
        }
        var clusterOptional = clusterRepository.findById(id);
        if(clusterOptional.isPresent()) {
            Cluster cluster = clusterOptional.get();
            result = Node.builder().id(id).type(EdgeChildType.CLUSTER).name(cluster.getName()).build();
            var productGroups = dateTime == null
                    ? groupRepository.getAllByParentId(id)
                    : clusterToGroupMap.getOrDefault(id, new ArrayList<>());
            result.setChildren(productGroups.stream().map(it -> {
                var products = dateTime == null
                        ? productRepository.getProductsByParentId(it.getId())
                        : groupToProductsMap.getOrDefault(it.getId(), new ArrayList<>());
                return Node.builder()
                        .id(it.getId())
                        .name(it.getName())
                        .type(EdgeChildType.PRODUCT_GROUP)
                        .children(
                                products.stream().map(
                                    product ->
                                    {
                                        return Node.builder()
                                                .id(product.getId())
                                                .name(product.getName())
                                                .type(EdgeChildType.PRODUCT)
                                                .build();
                                    }
                                )
                                .collect(Collectors.toList())
                        )
                        .build();
                }).collect(Collectors.toList()));
        }
        return result;
    }
}
