package ru.iteco.task2.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.iteco.task2.model.GraphUpdateRequest;
import ru.iteco.task2.model.Node;
import ru.iteco.task2.repository.ClusterRepository;
import ru.iteco.task2.repository.GroupRepository;
import ru.iteco.task2.repository.ProductRepository;
import ru.iteco.task2.service.GraphUpdateService;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@RestController
@RequiredArgsConstructor
@RequestMapping("/graph")
@Api(value = "Интерфейс для взаимодействия с графом")
@Log4j2
public class GraphController {
    private GraphUpdateService graphUpdateService;

    @Autowired
    public GraphController(GraphUpdateService graphUpdateService) {
       this.graphUpdateService = graphUpdateService;
    }


    @ApiOperation("Получение графа по идентификатору корневой вершины")
    @GetMapping(produces = {"application/json"})
    public ResponseEntity<Node> getGraph(@ApiParam(value = "id", required = true) Long id,
                                         @ApiParam(value = "date", required = false) LocalDateTime date) {
        return ResponseEntity.ok(graphUpdateService.getGraph(id, date));
    }

    @ApiOperation("Изменение связей графа")
    @PostMapping(value = "/edges", consumes = {"application/json"}, produces = {"application/json"})
    public ResponseEntity<Void> updateGraph(@RequestBody GraphUpdateRequest graphUpdateRequest) {
        graphUpdateService.updateGraph(graphUpdateRequest);
        return ResponseEntity.ok().build();
    }
}
