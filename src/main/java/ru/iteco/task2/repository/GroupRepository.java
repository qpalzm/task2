package ru.iteco.task2.repository;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.iteco.task2.entity.ProductGroup;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Repository
public interface GroupRepository extends CrudRepository<ProductGroup, Long> {
    List<ProductGroup> getAllByParentId(Long parentId);

    Optional<ProductGroup> getById(Long id);

    @Modifying
    @Query("update productGroup p set p.parentId = :parentId where p.id = :id")
    void updateParentIdById(@Param(value = "id") long id, @Param(value = "parentId") Long parentId);

    @Query(value = "with changed_rows as (\n" +
            "select pgh1.* \n" +
            "from product_group_history pgh1 JOIN(\n" +
            "SELECT ph.product_group_id, min(ph.updated) as updated \n" +
            "FROM product_group_history ph \n" +
            "where ph.updated >=?1 group by ph.product_group_id\n" +
            ") tmp ON tmp.product_group_id = pgh1.product_group_id AND tmp.updated = pgh1.updated\n" +
            ") \n" +
            "\n" +
            "select pg.* \n" +
            "from product_group pg where not exists (select * from changed_rows cr where cr.product_group_id = pg.id)\n" +
            "union \n" +
            "select changed_rows.product_group_id as id, changed_rows.\"name\", changed_rows.parent_id \n" +
            "from changed_rows", nativeQuery = true)
    List<ProductGroup> getProductGroupsByDate(LocalDateTime dateTime);
}
