CREATE TABLE product_group_history
(
    id     bigserial PRIMARY KEY,
    product_group_id int8 NOT NULL,
    parent_id int8 NOT NULL,
    updated timestamp,
    "name" varchar NULL
);

INSERT INTO product_group_history (product_group_id,parent_id,updated,"name") VALUES
(7,2,'2021-03-29 15:09:04','bags');

CREATE TABLE product_history
(
    id     bigserial PRIMARY KEY,
    product_id     int8 NOT NULL,
    parent_id int8 NOT NULL,
    updated timestamp,
    "name" varchar NULL,
    amount int8 NULL,
    price  numeric(10,2) NULL
);

INSERT INTO product_history (product_id,parent_id,updated,"name",amount,price) VALUES
(1,4,'2021-03-29 15:09:04.984415','butter',1,100.00),
(5,9,'2021-03-31 15:14:30.019315','dress',5,123.00),
(5,3,'2021-04-01 10:14:30.019315','new dress',1,100.00);