package ru.iteco.task2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;

@EnableJpaRepositories(basePackages = "ru.iteco.task2.repository")
@SpringBootApplication
@EnableAuthorizationServer
@EnableResourceServer
public class Task2Application {
	public static void main(String[] args) {
		SpringApplication.run(Task2Application.class, args);
	}
}
