package ru.iteco.task2.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.iteco.task2.entity.Cluster;
import ru.iteco.task2.entity.Product;
import ru.iteco.task2.entity.ProductGroup;
import ru.iteco.task2.repository.ClusterRepository;
import ru.iteco.task2.repository.GroupRepository;
import ru.iteco.task2.repository.ProductRepository;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/clusters")
@Api(value = "/clusters")
@Log4j2
public class GraphNodesController {
    private GroupRepository groupRepository;
    private ClusterRepository clusterRepository;
    private ProductRepository productRepository;

    @Autowired
    public GraphNodesController(GroupRepository groupRepository,
                                ClusterRepository clusterRepository,
                                ProductRepository productRepository) {
        this.groupRepository = groupRepository;
        this.clusterRepository = clusterRepository;
        this.productRepository = productRepository;
    }

    @ApiOperation("Получение вершин уровня кластеров")
    @GetMapping(value = "", produces = {"application/json"})
    public ResponseEntity<List<Cluster>> getClusters() {
        return ResponseEntity.ok(clusterRepository.findAll());
    }

    @ApiOperation("Получение вершин уровня групп")
    @GetMapping(value = "/{id}/groups", produces = {"application/json"})
    public ResponseEntity<List<ProductGroup>> getGroups(
            @ApiParam(value = "", required = true)
            @PathVariable("id") Long clusterId
        ) {
        return ResponseEntity.ok(groupRepository.getAllByParentId(clusterId));
    }

    @ApiOperation("Получение вершин уровня продуктов")
    @GetMapping(value = "/{id}/groups/{groupId}/products", produces = {"application/json"})
    public ResponseEntity<List<Product>> getProducts(
            @ApiParam(value = "", required = true)
            @PathVariable("id") Long clusterId,
            @ApiParam(value = "", required = true)
            @PathVariable("groupId") Long groupId
        ) {
        return ResponseEntity.ok(productRepository.getProductsByParentId(groupId));
    }
}
