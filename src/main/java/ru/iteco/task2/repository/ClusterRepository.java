package ru.iteco.task2.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.iteco.task2.entity.Cluster;

import java.util.List;
import java.util.Optional;

@Repository
public interface ClusterRepository extends CrudRepository<Cluster, Long> {
    List<Cluster> findAll();

    Optional<Cluster> findById(Long id);
}
