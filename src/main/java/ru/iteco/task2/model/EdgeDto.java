package ru.iteco.task2.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@ApiModel(description = "")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class EdgeDto {
    @ApiModelProperty("childId")
    private Long childId;

    @ApiModelProperty("parentId")
    private Long parentId;
}
