package ru.iteco.task2.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.iteco.task2.entity.Product;
import ru.iteco.task2.entity.ProductHistory;

import java.time.LocalDateTime;
import java.util.List;

@Repository
public interface ProductHistoryRepository extends CrudRepository<ProductHistory, Long> {
}
