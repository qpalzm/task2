package ru.iteco.task2.testcontainers;

import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest(properties = {
        "spring.datasource.url=jdbc:tc:postgresql:///task2?currentSchema=public5"
})
public abstract class AbstractContainerBaseTest {
}
